﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf_mvvm_sample
{
    public class AViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        string m_sampleData;

        public string SampleData
        {
            get
            {
                return m_sampleData;
            }
            set
            {
                m_sampleData = value;
                PropertyChanged?.Invoke(
                    this,
                    new PropertyChangedEventArgs(nameof(SampleData)));
            }
        }

        public AViewModel()
        {
            SampleData = "My name is ivan";
        }
    }
}
