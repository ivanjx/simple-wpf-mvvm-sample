﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace wpf_mvvm_sample
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        bool m_isAVisible;
        bool m_isBVisible;
        bool m_isCVisible;

        public bool IsAVisibile
        {
            get
            {
                return m_isAVisible;
            }
            set
            {
                m_isAVisible = value;
                PropertyChanged?.Invoke(
                    this,
                    new PropertyChangedEventArgs(nameof(IsAVisibile)));
            }
        }

        public bool IsBVisibile
        {
            get
            {
                return m_isBVisible;
            }
            set
            {
                m_isBVisible = value;
                PropertyChanged?.Invoke(
                    this,
                    new PropertyChangedEventArgs(nameof(IsBVisibile)));
            }
        }

        public bool IsCVisibile
        {
            get
            {
                return m_isCVisible;
            }
            set
            {
                m_isCVisible = value;
                PropertyChanged?.Invoke(
                    this,
                    new PropertyChangedEventArgs(nameof(IsCVisibile)));
            }
        }

        public ICommand ShowACommand
        {
            get;
            private set;
        }

        public ICommand ShowBCommand
        {
            get;
            private set;
        }

        public ICommand ShowCCommand
        {
            get;
            private set;
        }

        public MainViewModel()
        {
            // By default A is visible.
            IsAVisibile = true;

            // Creating commands.
            ShowACommand = new RelayCommand(_ => ShowA());
            ShowBCommand = new RelayCommand(_ => ShowB());
            ShowCCommand = new RelayCommand(_ => ShowC());
        }

        void ShowA()
        {
            IsAVisibile = true;
            IsBVisibile = false;
            IsCVisibile = false;
        }

        void ShowB()
        {
            IsAVisibile = false;
            IsBVisibile = true;
            IsCVisibile = false;
        }

        void ShowC()
        {
            IsAVisibile = false;
            IsBVisibile = false;
            IsCVisibile = true;
        }
    }
}
