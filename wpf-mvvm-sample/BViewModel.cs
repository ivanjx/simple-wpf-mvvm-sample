﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf_mvvm_sample
{
    public class BViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        int m_counter;

        public int Counter
        {
            get
            {
                return m_counter;
            }
            set
            {
                m_counter = value;
                PropertyChanged?.Invoke(
                    this,
                    new PropertyChangedEventArgs(nameof(Counter)));
            }
        }

        public BViewModel()
        {
            Start();
        }

        async void Start()
        {
            while (true)
            {
                Counter++;
                await Task.Delay(1000);
            }
        }
    }
}
